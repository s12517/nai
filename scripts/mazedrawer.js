function generateMaze(){
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.fillStyle = "#22680c";
    var tileSize = 14;
    var maze = [];
    var mazeWidth = 41;
    var mazeHeight = 41;


    var moves = [];
    for (var i = 0; i < mazeHeight; i++) {
        maze[i] = [];
        for (var j = 0; j < mazeWidth; j++) {
            maze[i][j] = 1;
        }
    }

    var posX = 1;
    var posY = 1;
    maze[posX][posY] = 0;
    moves.push(posY + posY * mazeWidth);
    while (moves.length) {
        var possibleDirections = "";
        if (posX + 2 > 0 && posX + 2 < mazeHeight - 1 && maze[posX + 2][posY] == 1) {
            possibleDirections += "S";
        }
        if (posX - 2 > 0 && posX - 2 < mazeHeight - 1 && maze[posX - 2][posY] == 1) {
            possibleDirections += "N";
        }
        if (posY - 2 > 0 && posY - 2 < mazeWidth - 1 && maze[posX][posY - 2] == 1) {
            possibleDirections += "W";
        }
        if (posY + 2 > 0 && posY + 2 < mazeWidth - 1 && maze[posX][posY + 2] == 1) {
            possibleDirections += "E";
        }
        if (possibleDirections) {

            var move = randomIntFromInterval(0, possibleDirections.length - 1);
            switch (possibleDirections[move]) {
                case "N":
                    maze[posX - 2][posY] = 0;
                    maze[posX - 1][posY] = 0;
                    posX -= 2;
                    break;
                case "S":
                    maze[posX + 2][posY] = 0;
                    maze[posX + 1][posY] = 0;
                    posX += 2;
                    break;
                case "W":
                    maze[posX][posY - 2] = 0;
                    maze[posX][posY - 1] = 0;
                    posY -= 2;
                    break;
                case "E":
                    maze[posX][posY + 2] = 0;
                    maze[posX][posY + 1] = 0;
                    posY += 2;
                    break;
            }
            moves.push(posY + posX * mazeWidth);
        }
        else {
            var back = moves.pop();
            posX = Math.floor(back / mazeWidth);
            posY = back % mazeWidth;
        }
    }
    drawMaze(posX, posY);
    var randPonint = c.getContext("2d");
    randPonint.fillStyle = "#FF00FF";
    point = pointGenerator();    
    randPonint.fillRect(point[0]* tileSize, point[1]* tileSize, tileSize, tileSize);
    var randPonint2 = c.getContext("2d");
    randPonint2.fillStyle = "#674da3";
    point2 = pointGenerator();    
    randPonint2.fillRect(point2[0]* tileSize, point2[1]* tileSize, tileSize, tileSize);


    function drawMaze(posX, posY) {

        for (i = 0; i < mazeHeight; i++) {
            for (j = 0; j < mazeWidth; j++) {
                if (maze[i][j] == 1) {

                    ctx.fillRect(j * tileSize, i * tileSize, tileSize, tileSize);
                }
            }
        }
    }

    function randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
    
        function heuristic(pos0, pos1) {

var dlX = Math.abs(pos1.x - pos0.x);
var dlY = Math.abs(pos1.y - pos0.y);
var length = Math.floor(Math.sqrt((dlX * dlX) + (dlY * dlY)));

return length;      
}

function pointGenerator() {
var i = 0;
while (i == 0) {
    var pointXtemp = Math.floor(Math.random() * mazeWidth) + 1;
    var pointYtemp = Math.floor(Math.random() * mazeHeight - 1) + 1;

    if (maze[pointYtemp][pointXtemp] == 0) {
        i++;
    }
}
return [pointXtemp, pointYtemp];
}

}